package m1.day5.objects_and_input;

import java.util.Locale;
import java.util.Scanner;
import java.util.SortedMap;

public class ObjectsAndInputMain {
    /* Break down PSVM again */
    /* Setup a run configuraiton to use arguments show arguments below via for loop */
    /* Maybe write an example JAR students can interact with? */
    /* Change args to be commandLineArguments, and make sure to note that this is only used when running a jar */
    /* Show args in debugger */
    public static void main(String[] args) {
        /* Getting user input via commandLineArgs isn't the best, as the user must have knowledge of the code
         * in order to provide the correct arguments */
        /* FIRST */
       // for(String arg : args) {
       //     System.out.println(arg);
      //  }
        /* SECOND */
        /* Why command line args aren't so great */
       // int guess = Integer.parseInt(args[0]);
       // guessMyFavoriteNumberCMD(guess);
        guessMyFavoriteNumber();
    }

    /* First add int scanner and boolean logic, give it a run */
    /* Second add user prompts */
    /* Third add while loop plus exit condition */
    /* Fourth improve exit condition, switch nextInt to nextLine */
    /* Fifth show number format exception, explore stack trace! */
    /* Sixth convert inputs to strings show how == fails!! */
    /* Seventh add nested if statement to catch E */
    public static void guessMyFavoriteNumber() {
        // Scanner allows us to read data from a specific source
        // This scanner constructor creates a new object whose data source is System.in (in short our keyboard)
        Scanner userInputScanner = new Scanner(System.in);
        boolean userWantsToExit = false;
        while(!userWantsToExit) {
            System.out.println("Guess my favorite number!! (Enter E to exit)");
            String guess = userInputScanner.nextLine(); // nextInt() waits for a user to type data into the console
            if(guess.equalsIgnoreCase("E")) {
                userWantsToExit = true;
                System.out.println("Goodbye!!");
            } else {
                if (guess.equals("17")) {
                    System.out.println("Yes that is my favorite number!");
                    System.out.println();
                } else {
                    System.out.println("No that is not my favorite number");
                    System.out.println();
                }
            }
        }
        userInputScanner.close();
    }

    /* To showcase static method calls write a summation method that adds a list of doubles, also review split!! */

    /* Use this as an example of why command line args aren't so great */
    public static void guessMyFavoriteNumberCMD(int guess) {
        if(guess == 17) {
            System.out.println("Yes that is my favorite number!");
        } else {
            System.out.println("No that is not my favorite number");
        }
    }
}
