package module1.day3.methods;

public class MethodsMain {
    /* Start with no main method, point out how our class definition is attached to a code block */
    /* Write out the mysterious main method, point out the method name and input parameters */
    /* Encourage students to write their own names as Strings */
    public static void main(String[] args) {
        /* Setup the fist name last name example from the slides */
        String firstName = "James";
        String lastName = "Epting";
        String fullName = firstName + " " + lastName;
        System.out.println(fullName);

        /* Give this a run just to show what the console output looks like, make a note that if we have other names
         * we want to create a full name for we would have to repeat the above code over and over again, time to write a method! (go write the method) */
        /* RETURN HERE, add method calls to show how quickly we can reuse our code vs copying and pasting */
        /* Show how this one runs, add more names, then change the method to be lastName, FirstName, note how the above implementation stays the same */
        // Methods allow us to reuse code
        printFullName("Tony", "Hawk");
        printFullName("Tony", "The Tiger");
        printFullName("Tony", "Stark");
        printFullName("Tony", "Soprano");

        /* For this section review how methods can also return something out of their call */
        /* Go write the method for returnFullName */
        /* Ask class for names in these examples */
        returnFullName("Speedy", "Gonzalez"); // Just creates a value that we arent using, IntelliJ even warns us about this!
        /* Name the variable fullName first, Show compiler error about the name collision here */
        String secondFullName = returnFullName("Ray", "Charles"); // We should store our return value in a variable
        String thirdFullName = returnFullName("Ash", "Ketchum");
        // String fourthFullName = printFullName("Bob", "The Builder"); // This does not work!!

        /* Note that grey variable names means they aren't being used! they are storing values though! */
        int sum = addTwoIntegers(4, 5);
        int product = multiplyThreeIntegers(12, 12, 2);
        double conversion = convertFeetToInches(6.0);
        double secondConversion = convertFeetToInches(4.5);
        int myFavoriteNumber = returnMyFavoriteNumber();

        /* Ask students what we could do to display the above values, answer: print! */
        /* Write out the Anatomy of the method the return to slides*/
    }


    /* Write this last! */
    // THE ANATOMY OF A METHOD
    // Public - This is called the accessor, specifies where our method can be called
    // Static - This is called the specifier, right now just know it is required to call our methods in the main method
    // Void - The Return type, can be any data type in Java, even a custom one, void returns nothing
    // printFullName - The method name, follows variable naming conventions
    // firstName, lastName - The input parameters, also optional, variables that will store values given to the method

    /* While writing this remember to remind students about formatting */

    /* Public - Means this method can be called anywhere in our project
     * Static - (Optional) Will be explained on a different day, but is required to run a method in our static main method!
     * Void - The return type, void means we are returning nothing
     * Stop at void and show returns in action, create second function
     * printFullName - Method name, follows variable naming conventions
     * firstName, lastName - Method parameters, also optional, the variables our user input gets stored in
     * NOTE HOW THE METHOD PARAMETERS DO NOT NEED TO MATCH THE VARIABLES USED AS ARGUMENTS */
    public static void printFullName(String firstName, String lastName) { // This line is called the method signature
        String fullName = firstName + " " + lastName;
        System.out.println(fullName);
    }

    /* 1. Show how the compiler error tells us we need to return something
     * 2. Show how the return type must match the data type of the variable we are returning
     * 3. Comment out the printFullName method call and run this in main instead, show how nothing happens
     * 4. Initialize a variable with this method call to show why return types are used
     * 5. (SLOWLY USE THE DEBUGGER) In the debugger show how this new variable is given the value from this method */
    public static String returnFullName(String firstName, String lastName) {
        String fullName = firstName + " " + lastName;
        return fullName;
    }

    /* First write this like so, then show how we can just return a + b */
    public static int addTwoIntegers(int a, int b) {
        int sum = a + b;
        return sum;
    }

    public static int multiplyThreeIntegers(int a, int b, int c) {
        return a * b * c;
    }

    public static double convertFeetToInches(double feet) {
        return feet * 12.0;
    }

    public static int returnMyFavoriteNumber() {
        return 17;
    }
}
