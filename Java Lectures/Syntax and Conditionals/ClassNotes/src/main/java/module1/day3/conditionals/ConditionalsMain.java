package module1.day3.conditionals;

public class ConditionalsMain {
    /* After all of this is done, go to LogicalOperatorsMain */
    public static void main(String[] args) {
        /* FOR THE REST OF THE DAY TODAY BREAK DOWN ALL OF THE METHOD SIGNATURES, SAY THINGS LIKE "LETS WRITE OUR METHOD SIGNATURE" */
        /* 1. Start off by recreating the space exploration scenario, "First we need to create our methods"
         * 2. Run with the return value of both false and true
         * 3. Show the console output
         * 4. Show how it looks in the debugger
         * 5. Add a conditional to scanPlanets to randomize the zerg detection
         * 6. Give new code a few runs to show how the logic works
         *
         * Transition to LogicalOperatorsMain */
        /* Here we can introduce stepping into a method with the debugger */
        boolean itIsSafeToExplore = scanPlanets();
        if(itIsSafeToExplore) { // English: If the expression in the parenthesis is...
            startExploring(); // ... True do this
        } else {
            comeHome(); // ... False do this
        }
    }

    public static boolean scanPlanets() {
        System.out.println("Scanning for ZERG...");
        // TODO ADD THIS AFTER WE GO THOUGH THE SIMPLE VERSION
        /* Lets improve our scanner so it can actually do something
           Here we will simulate a scan with some randomness */
        // Math.random() returns a random value from 0.0 to 1.0
        // This line of code creates a 50/50 chance of "finding" zerg
        /* We add 0.5 to the result of Math.random() to emulate a 50/50 chance between getting a value of 0 or 1 */
        int numberOfZergFound = (int)(Math.random() + 0.5); /* Show compiler error, ask class how can we convert from a double to an int */
        /* Make sure to mention the > sign is a logical operator */
        boolean thereAreZergPresent = numberOfZergFound > 0; /* Show it like this first, then move the comparison to the if statement */
        /* If we find at least 1 zerg, we should print a message and return false
         * we can check for at least 1 zerg by using a logical operator */
        if(thereAreZergPresent) {
            // This is the true code block, it only runs if the boolean value in our if statement is true
            System.out.println(numberOfZergFound + " ZERG has been found!");
            return false;
        } else {
            // This is the false code block, it only runs if the boolean value in our if statement is false
            System.out.println("No ZERG have been found!");
            return true;
        }
        /* Show compiler error, explain how return stops the current code execution in the method and "returns" to
         * where the method was called, show this in the debugger */
        // TODO ADD THIS AFTER WE GO THOUGH THE SIMPLE VERSION
//        return false;
    }

    public static void startExploring() {
        System.out.println("Beep boop, I'm exploring!");
    }

    public static void comeHome() {
        System.out.println("Boop beep, On my way back!");
    }
}
