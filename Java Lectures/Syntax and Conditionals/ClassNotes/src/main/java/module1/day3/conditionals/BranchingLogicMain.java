package module1.day3.conditionals;

public class BranchingLogicMain {
    public static void main(String args[]) {
        /* Start with the translation of one color*/
        String colorInSpanish = translateColorToSpanish("red");
        System.out.println("Teal in spanish is " + colorInSpanish);
    }

    /* Start with the translation of one color, explain how if doesn't require an else, show in debugger
       add if-else, add switch case */
    public static String translateColorToSpanish(String color) {
        String translatedColor = "";
        /* For now just tell students to use .equals() instead of == with Strings, say you will explain later, call on a student to repeat this
         * 1. Write out eat individual if else block
         * 2. Show in the debugger how each statement is evaluated
         * 3. Show how the code can be re-written with if-else, but no else statement
         * 4. Show how the last else block is the default case if everything fails
         * 5. Show how all of this can be converted to switch-case
         * 6. Note how switch case only does equivalence comparisons */
        // Always use .equals() when comparing strings!!

        if(color.equals("red")) {
            translatedColor = "rojo";
        }

        if(color.equals("yellow")) {
            translatedColor = "amarillo";
        }

        if(color.equals("blue")) {
            translatedColor = "azul";
        }

        /* Show missing return statement error, explain how if the color isn't teal, then our code has no where to go */
        return translatedColor;
    }
}
