package module1.day3.conditionals;

public class LogicalOperatorsMain {
    public static void main(String[] args) {
        int number = 10;

        /* Start with single boolean expressions */
        /* Write the if-statement after writing the first boolean, but keep it below all of the variables */
        // These are single boolean expressions, only one comparison is done
        boolean numberIsADozen = number == 12; // note '==' (equal to) is not '=' (value assignment)
        boolean numberIsLessThan100 = number < 100; // less than
        boolean numberIsLessThanOrEqualTo10 = number <= 10; // less than or equal to
        boolean numberIsGreaterThan44 = number > 44; // greater than
        boolean numberIsGreaterThanOrEqualTo0 = number >= 0; // greater than or equal to
        boolean numberIsNotEqualTo5 = number != 5; // not equal to /* Ask students to predict what this will print out */
        boolean falseValue = false;
        boolean logicalNotExample = !falseValue; // The '!' symbol (logical not) flips a boolean value to its opposite

        /* Use this to show how the boolean expressions work, play around with the values, make sure to mention this is
        * the beginner way to write boolean logic, you don;t need to use variables, you can just move the boolean expressions
        * into the if-statement */
        /* Use &&, ||, and ^ to showcase how they work with the above variables */
        if(numberIsADozen) {
            System.out.println("The boolean expression is true!");
        } else {
            System.out.println("The boolean expression is false!");
        }

        /* After this move to BranchingLogicMain */

    }
}
